# recipe-app-api-proxy

NGINX proxy for our recipe app API

## Usage

### Environent Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requeests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)


